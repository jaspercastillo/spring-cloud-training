package com.jc.javasig.spring.rest.ex02;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@SpringBootApplication
public class PingRestService {

    @Value("${message}")
    private String message;

    @Value("${message2:hello world!}")
    private String message2;

    @RequestMapping("/")
    public Map<String,Object> ping() {
        return Collections.singletonMap("message", message);
    }

    @RequestMapping("/hello")
    public Map<String,Object> hello() {
        return Collections.singletonMap("message", message2);
    }

    public static void main(String[] args) {
        SpringApplication.run(PingRestService.class, args);
    }
}
