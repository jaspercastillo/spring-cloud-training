package com.jc.javasig.spring.rest.ex01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@SpringBootApplication
public class PingRestService {

    @RequestMapping("/")
    public Map<String,Object> ping() {
        return Collections.singletonMap("message", "pong");
    }

    public static void main(String[] args) {
        SpringApplication.run(PingRestService.class, args);
    }
}
